$(document).ready(function () {

  $(document).foundation();

  /* sidebar functions */
  $(document).on('click','.js-toggleSidebar', function(){
    $('.sidebar').toggleClass('sidebar-closed');
    $('.sidebar-overlay').fadeToggle( 300 );
    $('body').toggleClass('no-scroll');
    $('.menu-toggle').fadeToggle( 300 );
  });


  /* Slick slider */
  $('.carousel').slick({
    infinite: true,
    // autoplay: true,
    autoplaySpeed: 3000,
    speed: 700,
    slidesToShow: 7,
    slidesToScroll: 1,
    prevArrow: $('.prev'),
    nextArrow: $('.next'),
    responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 5
      }
    },
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 3
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1
      }
    }
    ]
  });


  $(".carousel .slick-slide.slick-active").each(function(i) {
    $(this).addClass("item-"+(i+1));
  });

  $('.carousel').on('beforeChange', function(event, slick, currentSlide, nextSlide){
    for (i = 1; i < 8; i++) {
      $(".carousel .slick-slide").removeClass("item-"+i);
    }
  });

  $('.carousel').on('afterChange', function(event, slick, currentSlide){
    $(".carousel .slick-slide.slick-active").each(function(i) {
      $(this).addClass("item-"+(i+1));
    });
  });


  /* Isotope sorting and filtering */
  // init Isotope
  var $grid = $('.grid').isotope({
    itemSelector: '.element-item',
    layoutMode: 'fitRows',
    getSortData: {
      time: '[data-time]',
      views: '[data-views]',
      name: '.grid-name'
    },
    sortAscending: {
      time: false,
      views: true,
      name: true
    }
  });


  //change is-checked class on buttons
  $('.button-group').each( function( i, buttonGroup ) {
    var $buttonGroup = $( buttonGroup );
    $buttonGroup.on( 'click', 'button', function() {
      $buttonGroup.find('.is-checked').removeClass('is-checked');
      $( this ).addClass('is-checked');
    });
  });


  // sort items on button click
  $('#filters-select').on( 'click', '.styled-select-dropdown > li', function() {
    var sortByValue = $(this).attr('data-value');
    console.log(sortByValue);
    $grid.isotope({ sortBy: sortByValue });
  });


  // filter items on button click
  $('.filter-button-group').on( 'click', '.button', function() {
    if ($(this).attr('data-filter') == 'all' ) {
      var filterValue = '*';
    } else{
      var filterValue = '.'+$(this).attr('data-filter');
    }
    $grid.isotope({ filter: filterValue });
  });


  // Styled select
  $('.styled-select-input').on('click', function(){
    $('.styled-select-dropdown').addClass('is-active');
  });
  $('.styled-select-dropdown li').on('click', function(){
    $('.styled-select-dropdown').removeClass('is-active');
  });


  // Styled select - add chosen data
  $(function() {
    var dd = new DropDown( $('#filters-select') );
  });

  function DropDown(el) {
    this.dd = el;
    this.placeholder = this.dd.children('span');
    this.opts = this.dd.find('ul.styled-select-menu > li');
    this.val = '';
    this.index = -1;
    this.initEvents();
  }
  DropDown.prototype = {
    initEvents : function() {
      var obj = this;

      obj.opts.click(function(){
        var opt = $(this);
        obj.val = opt.text();
        obj.index = opt.index();
        obj.placeholder.text(obj.val);
      });
    },
    getValue : function() {
      return this.val;
    },
    getIndex : function() {
      return this.index;
    }
  }


  // Cube transformations
  $('.scene .left').on('click', function(){
    $('.scene').removeClass('scene-front scene-right scene-back').addClass('scene-left');
  });

  $('.scene .right').on('click', function(){
    $('.scene').removeClass('scene-front scene-left scene-back').addClass('scene-right');
  });

  $('.scene .front').on('click', function(){
    $('.scene').removeClass('scene-left scene-right scene-back').addClass('scene-front');
  });

  $('.scene .back').on('click', function(){
    $('.scene').removeClass('scene-front scene-right scene-left').addClass('scene-back');
  });

  $('.scene .side').on('click', function(){
    if ( !($(this).hasClass('active')) ) {
      $('.scene .side').removeClass('active');
      $(this).addClass('active');
    }
  });


  // Main menu click events
  $('.main-menu a').on('click', function(){
    $('.main-menu li').removeClass('active');
    $(this).parent().addClass('active');
  });

  $('.js-schedule').on('click', function(){
    $('.scene').removeClass('scene-left scene-front scene-back').addClass('scene-right');

    if ( !($('.scene .side').hasClass('active')) ) {
      $('.scene .side').removeClass('active');
      $('.scene-right .side').addClass('active');
    }
  });

  $('.js-comments').on('click', function(){
    $('.scene').removeClass('scene-right scene-front scene-back').addClass('scene-left');

    if ( !($('.scene .side').hasClass('active')) ) {
      $('.scene .side').removeClass('active');
      $('.scene-left .side').addClass('active');
    }
  });

  $('.js-video').on('click', function(){
    $('.scene').removeClass('scene-right scene-left scene-front').addClass('scene-front');

    if ( !($('.scene .side.front').hasClass('active')) ) {
      $('.scene .side').removeClass('active');
      $('.scene .side.front').addClass('active');
    }
  });

  $('.js-news').on('click', function(){
    $('.scene').removeClass('scene-right scene-left scene-front').addClass('scene-back');

    if ( !($('.scene .side.back').hasClass('active')) ) {
      $('.scene .side').removeClass('active');
      $('.scene .side.back').addClass('active');
    }
  });


  //Main menu sections on screens below 1024px
  var width = $(window).width();
  if( width < 1024 ){

    $('.js-video').on('click', function(){
      $('.scene .side:not(.front)').hide();
      $('.scene .side.front').show();
    });

    $('.js-schedule').on('click', function(){
      $('.scene .side:not(.right)').hide();
      $('.scene .side.right').show();
    });

    $('.js-comments').on('click', function(){
      $('.scene .side:not(.left)').hide();
      $('.scene .side.left').show();
    });

    $('.js-news').on('click', function(){
      $('.scene .side:not(.back)').hide();
      $('.scene .side.back').show();
    });

  }


});

